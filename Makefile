.PHONY: usage view all clean

usage:
	@echo "usage: make [clean|build]"

view: proposal.pdf
	open proposal.pdf

all: proposal.pdf

clean:
	rm -f {proposal,project}.{aux,dvi,log,out,pdf}

%.pdf: %.dvi
	dvipdf $<

%.dvi: %.tex
	latex $<

